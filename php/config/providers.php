<?php

return [
    \App\Provider\AuthServiceProvider::class,
    \App\Provider\RepositoryProvider::class,
    \App\Provider\DatabaseServiceProvider::class,
];