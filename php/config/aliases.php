<?php

return [
    'class_aliases' => [
        '\Auth' => \Framework\Facade\Auth::class,
    ],
    'short_aliases' => [
        'auth' => \App\Http\Middleware\AuthorizationMiddleware::class,
        'admin' => \App\Http\Middleware\AdminMiddleware::class,

        // validation rules
        'required' => \Framework\Support\Validator\Rules\Required::class,
        'same' => \Framework\Support\Validator\Rules\Same::class,
        'unique' => \Framework\Support\Validator\Rules\Unique::class,
        'mail' => \Framework\Support\Validator\Rules\Mail::class,
    ]
];