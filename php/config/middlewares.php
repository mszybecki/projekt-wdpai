<?php

use App\Http\Middleware\TrimStringsMiddleware;

return [
    TrimStringsMiddleware::class,
];