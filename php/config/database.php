<?php

use Framework\Core\Database\PostgresDriver;

return [
    'host' => env('database.host', 'postgres'),
    'port' => env('database.port', 5432),
    'user' => env('database.user', 'postgres'),
    'password' => env('database.password', 'postgres'),
    'database' => env('database.database', 'wdpai'),
    'autocommit' => env('database.autocommit', false),

    'driver' => env('database.driver', PostgresDriver::class),
];