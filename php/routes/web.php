<?php

use App\Http\Controller\{HelloController, AuthController, HomeController, BetController};
use App\Http\Controller\Admin\{PanelController, UserController as UserAdminController, BetController as BetAdminController, CategoryController as CategoryAdminController, TeamController as TeamAdminController};
use Framework\Facade\Router;

Router::get('/', HomeController::class . '@index');

Router::get('/user', HelloController::class . '@hello', ['auth']);
Router::patch('/user/{id}', HelloController::class . '@update', ['auth']);

Router::get('/login', AuthController::class . '@loginForm');
Router::post('/login', AuthController::class . '@login');

Router::get('/logout', AuthController::class . '@logout');

Router::get('/register', AuthController::class . '@registerForm');
Router::post('/register', AuthController::class . '@register');

Router::get('/zaklady', BetController::class . '@index');
Router::get('/zaklady/kategorie/{slug}', BetController::class . '@indexBySlug');
Router::get('/zaklady/{id}', BetController::class . '@show');
Router::post('/zaklady/{id}', BetController::class . '@addUserBet', ['auth',]);

// admin
Router::get('/panel', PanelController::class . '@index', ['auth', 'admin',]);
Router::get('/panel/uzytkownicy', UserAdminController::class . '@index', ['auth', 'admin',]);
Router::delete('/panel/uzytkownicy/{id}', UserAdminController::class . '@delete', ['auth', 'admin',]);
Router::get('/panel/uzytkownicy/{id}/zmien-role', UserAdminController::class . '@changeRole', ['auth', 'admin',]);

Router::get('/panel/zaklady', BetAdminController::class . '@index', ['auth', 'admin',]);
Router::get('/panel/zaklady/nowy', BetAdminController::class . '@viewForm', ['auth', 'admin',]);
Router::post('/panel/zaklady/nowy', BetAdminController::class . '@store', ['auth', 'admin',]);
Router::delete('/panel/zaklady/{id}', BetAdminController::class . '@delete', ['auth', 'admin',]);

Router::get('/panel/kategorie', CategoryAdminController::class . '@index', ['auth', 'admin',]);
Router::get('/panel/kategorie/nowy', CategoryAdminController::class . '@viewForm', ['auth', 'admin',]);
Router::post('/panel/kategorie/nowy', CategoryAdminController::class . '@store', ['auth', 'admin',]);
Router::delete('/panel/kategorie/{id}', CategoryAdminController::class . '@delete', ['auth', 'admin',]);

Router::get('/panel/zespoly', TeamAdminController::class . '@index', ['auth', 'admin',]);
Router::get('/panel/zespoly/nowy', TeamAdminController::class . '@viewForm', ['auth', 'admin',]);
Router::post('/panel/zespoly/nowy', TeamAdminController::class . '@store', ['auth', 'admin',]);
Router::delete('/panel/zespoly/{id}', TeamAdminController::class . '@delete', ['auth', 'admin',]);