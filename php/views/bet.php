{% extends template %}

{% block title %}
Zakłady
{% endblock %}

{% block content %}
<div class="bets--holder">
    <div class="bets--categories">
        <p class="bets--categories__title"><a href="/zaklady">Kategorie</a></p>
        <p class="bets--categories--mobile__title"><a href="/zaklady">Kategorie</a></p>
        <div class="bets--categories--links">
            {% foreach($categories as $category): %}
                <a href="/zaklady/kategorie/{{ $category->slug }}" class="bets--categories__anchor">{{ $category->name }}</a>
            {% endforeach; %}
        </div>
    </div>
    <div class="bets--calendar">
        {% foreach($vbets as $date => $betss): %}
            <div class="bets">
                <div class="bets--date">
                    <p>{{ $date }}</p>
                    <div class="bets--date--arrow"></div>
                </div>
                {% foreach($betss as $bet): %}
                <a href="/zaklady/{{ $bet->id }}">
                    <div class="bet">
                        <div class="bet--time"><p>{{ $bet->time }}</p></div>
                        <div class="bet--name"><p>{{ $bet->name }}</p></div>
                    </div>
                </a>
                {% endforeach; %}
            </div>
        {% endforeach; %}
        {% if (empty($vbets)): %}
            <div class="warning">
                <p>Nie ma zakładów w danej kategorii</p>
            </div>
        {% endif; %}
    </div>
    <div class="bets--ads">

    </div>
</div>
{% endblock %}