{% extends template %}

{% block title %}
Hello
{% endblock %}

{% block content %}
<div class="panel">
    <div class="panel--calendar" style="width: 100%">
        <div class="form form__panel">
            <form action="/user/{{ $user->id }}" method="post">
                <input type="hidden" name="_method" value="patch">
                <div class="form--group">
                    <label for="firstname">Imię</label>
                    <input type="text" name="firstname" id="firstname" value="{{ $vuser->firstname }}">
                    {% if(isset($errors['firstname'])): %}
                    <div class="form--error">
                        <p>{{ $errors['firstname'] }}</p>
                    </div>
                    {% endif; %}
                </div>
                <div class="form--group">
                    <label for="lastname">Nazwisko</label>
                    <input type="text" name="lastname" id="lastname" value="{{ $vuser->lastname }}">
                    {% if(isset($errors['lastname'])): %}
                    <div class="form--error">
                        <p>{{ $errors['lastname'] }}</p>
                    </div>
                    {% endif; %}
                </div>
                <input type="submit" value="Dodaj">
            </form>
        </div>
    </div>
</div>
{% endblock %}