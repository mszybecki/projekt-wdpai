{% extends panel.panel %}

{% block panel %}
    <div class="panel--user">
        <table>
            <thead>
                <tr>
                    <td>id</td>
                    <td>login</td>
                    <td>email</td>
                    <td>rola</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                {% foreach($users as $user): %}
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->login }}</td>
                        <td>{{ $user->email }}</td>
                        <td><a href="/panel/uzytkownicy/{{ $user->id }}/zmien-role">{{ $user->role }}</a></td>
                        <td>
                            <form action="/panel/uzytkownicy/{{ $user->id }}" method="post">
                                <input type="hidden" name="_method" value="delete">
                                <button type="submit" class="button--delete">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </button>
                            </form>
                        </td>
                    </tr>
                {% endforeach; %}
            </tbody>
        </table>
        <div class="pagination--buttons">
            {% if(isset($prev)): %}
                <p><a href="{{ $prev }}">Wcześniej</a></p>
            {% endif; %}
            <div></div>
            {% if(isset($next)): %}
                <p><a href="{{ $next }}">Dalej</a></p>
            {% endif; %}
        </div>
    </div>
{% endblock %}