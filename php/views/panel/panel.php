{% extends template %}

{% block title %}
Panel
{% endblock %}

{% block content %}
<div class="panel">
    <div class="panel--categories">
        <p class="panel--categories__title"><a href="/panel">Panel</a></p>
        <a href="/panel/uzytkownicy" class="panel--categories__anchor">Użytkownicy</a>
        <a href="/panel/zaklady" class="panel--categories__anchor">Zakłady</a>
        <a href="/panel/kategorie" class="panel--categories__anchor">Kategorie</a>
        <a href="/panel/zespoly" class="panel--categories__anchor">Zespoły</a>
    </div>
    <div class="panel--calendar">
        {% yield panel %}
    </div>
</div>
{% endblock %}