{% extends panel.panel %}

{% block panel %}
<div class="form form__panel">
    <form action="/panel/kategorie/nowy" method="post">
        <div class="form--group">
            <label for="name">Nazwa</label>
            <input type="text" name="name" id="name">
            {% if(isset($errors['name'])): %}
            <div class="form--error">
                <p>{{ $errors['name'] }}</p>
            </div>
            {% endif; %}
        </div>
        <input type="submit" value="Dodaj">
    </form>
</div>
{% endblock %}