{% extends panel.panel %}

{% block panel %}
    <div class="panel--user">
        <table>
            <thead>
                <tr>
                    <td>id</td>
                    <td>nazwa</td>
                    <td>slug</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                {% foreach($categories as $category): %}
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->slug }}</td>
                        <td>
                            <form action="/panel/kategorie/{{ $category->id }}" method="post">
                                <input type="hidden" name="_method" value="delete">
                                <button type="submit" class="button--delete">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </button>
                            </form>
                        </td>
                    </tr>
                {% endforeach; %}
            </tbody>
        </table>
        <div class="panel--new">
            <a href="/panel/kategorie/nowy">dodaj</a>
        </div>
        <div class="pagination--buttons">
            {% if(isset($prev)): %}
                <p><a href="{{ $prev }}">Wcześniej</a></p>
            {% endif; %}
            <div></div>
            {% if(isset($next)): %}
                <p><a href="{{ $next }}">Dalej</a></p>
            {% endif; %}
        </div>
    </div>
{% endblock %}