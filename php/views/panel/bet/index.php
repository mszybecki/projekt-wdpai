{% extends panel.panel %}

{% block panel %}
    <div class="panel--user">
        <table>
            <thead>
                <tr>
                    <td>id</td>
                    <td>nazwa</td>
                    <td>data</td>
                    <td>czas</td>
                    <td>kategoria</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                {% foreach($bets as $bet): %}
                    <tr>
                        <td>{{ $bet->id }}</td>
                        <td>{{ $bet->name }}</td>
                        <td>{{ $bet->date }}</td>
                        <td>{{ $bet->time }}</td>
                        <td>{{ $bet->category->name }}</td>
                        <td>
                            <form action="/panel/zaklady/{{ $bet->id }}" method="post">
                                <input type="hidden" name="_method" value="delete">
                                <button type="submit" class="button--delete">
                                    <ion-icon name="trash-outline"></ion-icon>
                                </button>
                            </form>
                        </td>
                    </tr>
                {% endforeach; %}
            </tbody>
        </table>
        <div class="panel--new">
            <a href="/panel/zaklady/nowy">dodaj</a>
        </div>
        <div class="pagination--buttons">
            {% if(isset($prev)): %}
                <p><a href="{{ $prev }}">Wcześniej</a></p>
            {% endif; %}
            <div></div>
            {% if(isset($next)): %}
                <p><a href="{{ $next }}">Dalej</a></p>
            {% endif; %}
        </div>
    </div>
{% endblock %}