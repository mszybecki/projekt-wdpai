{% extends panel.panel %}

{% block panel %}
<div class="form form__panel">
    <form action="/panel/zaklady/nowy" method="post">
        <div class="form--group">
            <label for="team_1_id">Zespół 1</label>
            <select name="team_1_id" id="team_1_id">
                {% foreach($teams as $team): %}
                <option value="{{ $team->id }}">{{ $team->name }}</option>
                {% endforeach %}
            </select>
            {% if(isset($errors['team_1_id'])): %}
            <div class="form--error">
                <p>{{ $errors['team_1_id'] }}</p>
            </div>
            {% endif; %}
        </div>
        <div class="form--group">
            <label for="team_2_id">Zespół 2</label>
            <select name="team_2_id" id="team_2_id">
                {% foreach($teams as $team): %}
                <option value="{{ $team->id }}">{{ $team->name }}</option>
                {% endforeach %}
            </select>
            {% if(isset($errors['team_2_id'])): %}
            <div class="form--error">
                <p>{{ $errors['team_2_id'] }}</p>
            </div>
            {% endif; %}
        </div>
        <div class="form--group">
            <label for="date">Data</label>
            <input type="date" name="date" id="date">
            {% if(isset($errors['date'])): %}
            <div class="form--error">
                <p>{{ $errors['date'] }}</p>
            </div>
            {% endif; %}
        </div>
        <div class="form--group">
            <label for="time">Godzina</label>
            <input type="text" name="time" id="time">
            {% if(isset($errors['time'])): %}
            <div class="form--error">
                <p>{{ $errors['time'] }}</p>
            </div>
            {% endif; %}
        </div>
        <div class="form--group">
            <label for="category">Kategoria</label>
            <select name="category_id" id="category_id">
                {% foreach($categories as $category): %}
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                {% endforeach %}
            </select>
            {% if(isset($errors['category_id'])): %}
            <div class="form--error">
                <p>{{ $errors['category_id'] }}</p>
            </div>
            {% endif; %}
        </div>
        <input type="submit" value="Dodaj">
    </form>
</div>
{% endblock %}