{% extends template %}

{% block title %}
Login form
{% endblock %}

{% block content %}

<div class="form form__login">
    <div class="form--holder">
        {% foreach($errors as $error): %}
        <div class="form--error">
            <p style="text-align: center">{{ $error }}</p>
        </div>
        {% endforeach; %}
        <form action="/login" method="post">
            <div class="form--group">
                <label for="username">Login</label>
                <input type="text" name="username" id="username">
            </div>
            <div class="form--group">
                <label for="password">Hasło</label>
                <input type="password" name="password" id="password">
            </div>
            <input type="submit" value="Zaloguj">
        </form>
    </div>
</div>

{% endblock %}