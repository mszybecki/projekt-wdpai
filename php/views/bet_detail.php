{% extends template %}

{% block title %}
Zakłady | {{ $bet->name }}
{% endblock %}

{% block content %}
<div class="bets--holder">
    <div class="bets--categories">
        <p class="bets--categories__title"><a href="/zaklady">Kategorie</a></p>
        <p class="bets--categories--mobile__title"><a href="/zaklady">Kategorie</a></p>
        <div class="bets--categories--links">
            {% foreach($categories as $category): %}
            <a href="/zaklady/kategorie/{{ $category->slug }}" class="bets--categories__anchor">{{ $category->name }}</a>
            {% endforeach; %}
        </div>
    </div>
    <div class="bets--calendar">
        <p class="bet--title">{{ $bet->name }}</p>
        {% if(\Auth::authenticated()): %}
            <form action="/zaklady/{{ $bet->id }}" method="post">
                <div class="bet--group">
                    <p>Aktualny stosunek</p>
                    <p class="bet--block">{{ $bet->ratio }}</p>
                </div>
                <div class="bet--group">
                    {% if(isset($errors['team_id'])): %}
                    <div class="form--error">
                        <p>{{ $errors['team_id'] }}</p>
                    </div>
                    {% endif; %}
                    <p>Obstaw wygraną dla:</p>
                    <div class="bet--checkboxes">
                        <div class="bet--checkbox">
                            <input type="radio" name="team_id" value="{{ $bet->team1->id }}" id="team1">
                            <label for="team1" class="bet--block">{{ $bet->team1->name }}</label>
                        </div>
                        <div class="bet--checkbox">
                            <input type="radio" name="team_id" value="{{ $bet->team2->id }}" id="team2">
                            <label for="team2" class="bet--block">{{ $bet->team2->name }}</label>
                        </div>
                    </div>
                </div>
                <div class="bet--group">
                    {% if(isset($errors['amount'])): %}
                    <div class="form--error">
                        <p>{{ $errors['amount'] }}</p>
                    </div>
                    {% endif; %}
                    <p>Kwotą</p>
                    <input type="text" name="amount" class="bet--block">
                    <input type="submit" value="Obstaw" class="bet--block">
                </div>
            </form>
        {% endif; %}
        <div class="bet--group bet--history">
            <p>Historia obstawiania:</p>
            {% foreach($userBets as $userBet): %}
                <p>{{ $userBet->user->login }} - {{ $userBet->team->name }} - {{ $userBet->amount }} zł</p>
            {% endforeach; %}
        </div>
    </div>
    <div class="bets--ads">

    </div>
</div>
{% endblock %}