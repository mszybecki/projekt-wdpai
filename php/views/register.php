{% extends template %}

{% block title %}
Login form
{% endblock %}

{% block content %}

<div class="form form__login">
    <div class="form--holder">
        <form action="/register" method="post">
            <div class="form--group">
                <label for="login">Login</label>
                <input type="text" name="login" id="login">
                {% if(isset($errors['login'])): %}
                <div class="form--error">
                    <p>{{ $errors['login'] }}</p>
                </div>
                {% endif; %}
            </div>
            <div class="form--group">
                <label for="email">Email</label>
                <input type="email" name="email" id="email">
                {% if(isset($errors['email'])): %}
                <div class="form--error">
                    <p>{{ $errors['email'] }}</p>
                </div>
                {% endif; %}
            </div>
            <div class="form--group">
                <label for="password">Hasło</label>
                <input type="password" name="password" id="password">
                {% if(isset($errors['password'])): %}
                <div class="form--error">
                    <p>{{ $errors['password'] }}</p>
                </div>
                {% endif; %}
            </div>
            <div class="form--group">
                <label for="repeat-password">Powtórz hasło</label>
                <input type="password" name="repeat-password" id="repeat-password">
                {% if(isset($errors['repeat-password'])): %}
                <div class="form--error">
                    <p>{{ $errors['repeat-password'] }}</p>
                </div>
                {% endif; %}
            </div>
            <input type="submit" value="Login">
        </form>
    </div>
</div>

{% endblock %}