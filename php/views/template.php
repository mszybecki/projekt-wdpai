<!DOCTYPE html>
<html lang="eng">

    <head>
        <title>Betify | {% yield title %}</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="/css/style.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="header">
            <h1><a href="/">Betify</a></h1>
            <div class="menu">
                <div class="menu__wide">
                    {% if(!\Auth::authenticated()): %}
                        <div><p><a href="/register">Załóż konto</a></p></div>
                        <p><a href="/login">Zaloguj</a></p>
                    {% else: %}
                        <p><a href="/logout">Wyloguj</a></p>
                        {% if(\Auth::getAuthenticated()->role == 'admin'): %}
                            <div><p><a href="/panel">Panel</a></p></div>
                        {% else: %}
                            <div><p><a href="">0,00 zł</a></p></div>
                        {% endif; %}
                        <p><a href="/user">{{ \Auth::getAuthenticated()->login }}</a></p>
                    {% endif; %}
                </div>
                <p id="mobile-menu-open" style="cursor: pointer">menu</p>
                <div class="menu__mobile">
                    {% if(!\Auth::authenticated()): %}
                        <p><a href="/register">Załóż konto</a></p>
                        <p><a href="/login">Zaloguj</a></p>
                    {% else: %}
                        <p><a href="/logout">Wyloguj</a></p>
                        {% if(\Auth::getAuthenticated()->role == 'admin'): %}
                            <p><a href="/panel">Panel</a></p>
                        {% endif; %}
                        <p><a href="/user">{{ \Auth::getAuthenticated()->login }}</a></p>
                    {% endif; %}
                </div>
            </div>
        </div>

        <div class="content">
            {% yield content %}
        </div>

        <div class="footer">
            <p>Betify</p>
            <p>&copy; Betify by Maciej Szybecki.</p>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="/js/script.js"></script>
        <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    </body>

</html>
