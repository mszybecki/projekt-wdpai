<?php

namespace App\Entity;

use App\Repository\Interfaces\BetRepositoryInterface;
use App\Repository\Interfaces\TeamRepositoryInterface;
use App\Repository\Interfaces\UserRepositoryInterface;
use Framework\Core\Database\Model;

class UserBet extends Model
{
    protected array $fillable = [
        'team_id',
        'bet_id',
        'user_id',
        'amount'
    ];

    protected function getTeamAttribute()
    {
        $container = container();
        $repository = $container->make(TeamRepositoryInterface::class);

        return $repository->findById($this->attributes['team_id']);
    }

    protected function getBetAttribute()
    {
        $container = container();
        $repository = $container->make(BetRepositoryInterface::class);

        return $repository->find($this->attributes['bet_id']);
    }

    protected function getUserAttribute()
    {
        $container = container();
        $repository = $container->make(UserRepositoryInterface::class);

        return $repository->get($this->attributes['user_id']);
    }
}