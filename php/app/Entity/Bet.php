<?php

namespace App\Entity;

use App\Repository\Interfaces\CategoryRepositoryInterface;
use App\Repository\Interfaces\TeamRepositoryInterface;
use Framework\Core\Database\Model;

class Bet extends Model
{
    protected array $fillable = [
        'team_1_id',
        'team_2_id',
        'category_id',
        'date',
        'time'
    ];

    public function getTeam1Attribute()
    {
        $container = container();
        $repository = $container->make(TeamRepositoryInterface::class);

        return $repository->findById($this->team_1_id);
    }

    public function getTeam2Attribute()
    {
        $container = container();
        $repository = $container->make(TeamRepositoryInterface::class);

        return $repository->findById($this->team_2_id);
    }

    public function getNameAttribute()
    {
        return "{$this->team_1->name} vs {$this->team_2->name}";
    }

    public function getCategoryAttribute()
    {
        $container = container();
        $repository = $container->make(CategoryRepositoryInterface::class);

        return $repository->findById($this->category_id);
    }

    public function getRatioAttribute()
    {
        return "Do zrobienia";
    }
}