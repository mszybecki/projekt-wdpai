<?php

namespace App\Entity;

use Framework\Core\Database\Model;

class Team extends Model
{
    protected array $fillable = [
        'name',
    ];
}