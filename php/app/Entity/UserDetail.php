<?php

namespace App\Entity;

use Framework\Core\Database\Model;

class UserDetail extends Model
{
    protected array $fillable = [
        'id',
        'firstname',
        'lastname',
    ];
}