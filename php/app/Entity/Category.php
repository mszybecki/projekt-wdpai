<?php

namespace App\Entity;

use App\Repository\Interfaces\BetRepositoryInterface;
use Framework\Core\Database\Model;

class Category extends Model
{
    protected array $fillable = [
        'name'
    ];
}