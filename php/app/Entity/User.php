<?php

namespace App\Entity;

use Framework\Core\Database\Model;
use Framework\Support\Hash;

class User extends Model
{
    protected array $fillable = [
        'login',
        'email',
        'password',
        'role',
    ];

    protected function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }
}