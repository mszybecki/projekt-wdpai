<?php

namespace App\Http\Middleware;

use Framework\Core\Middleware;
use Framework\Facade\Response;
use Framework\Facade\Auth;

class AuthorizationMiddleware extends Middleware
{
    public function handle()
    {
        if (!Auth::authenticated()) {
            return Response::redirect()
                ->setRedirect(Auth::getRedirect())
                ->setStatusCode(403);
        }
    }
}