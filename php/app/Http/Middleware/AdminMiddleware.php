<?php

namespace App\Http\Middleware;

use Framework\Core\Middleware;
use Framework\Facade\Auth;
use Framework\Facade\Response;

class AdminMiddleware extends Middleware
{
    public function handle()
    {
        $user = Auth::getAuthenticated();

        if ($user->role != 'admin') {
            return Response::view()
                ->setName('error')
                ->setStatusCode(403)
                ->setParameters(['status_code' => 403]);
        }
    }
}