<?php

namespace App\Http\Middleware;

use Framework\Core\Middleware;
use Framework\Core\Request;

class TrimStringsMiddleware extends Middleware
{
    /**
     * @var Request
     */
    private $request;

    /**
     * TrimStringsMiddleware constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $inputs = $this->request->getInputs();

        foreach ($inputs as $key => $value) {
            $value = trim($value);

            $this->request->setInput($key, $value);
        }
    }
}