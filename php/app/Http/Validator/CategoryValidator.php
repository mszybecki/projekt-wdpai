<?php

namespace App\Http\Validator;

use Framework\Support\Validator\Validator;

class CategoryValidator extends Validator
{
    public function getRules()
    {
        return [
            'name' => [
                'required',
                'unique:categories',
            ]
        ];
    }

    public function getMessages()
    {
        return [
            'name' => [
                'required' => 'Nazwa kategorii jest wymagana',
                'unique' => 'Taka nazwa już istnieje',
            ]
        ];
    }
}