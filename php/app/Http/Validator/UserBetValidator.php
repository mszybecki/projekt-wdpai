<?php

namespace App\Http\Validator;

use Framework\Support\Validator\Validator;

class UserBetValidator extends Validator
{
    public function getRules()
    {
        return [
            'team_id' => [
                'required',
            ],
            'amount' => [
                'required',
            ],
        ];
    }

    public function getMessages()
    {
        return [
            'team_id' => [
                'required' => 'Zespół jest wymagany',
            ],
            'amount' => [
                'required' => 'Kwota jest wymagana',
            ],
        ];
    }
}