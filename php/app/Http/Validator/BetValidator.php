<?php

namespace App\Http\Validator;

use Framework\Support\Validator\Validator;

class BetValidator extends Validator
{
    public function getRules()
    {
        return [
            'team_1_id' => [
                'required',
            ],
            'team_2_id' => [
                'required',
            ],
            'date' => [
                'required',
            ],
            'time' => [
                'required',
            ],
            'category_id' => [
                'required',
            ],
        ];
    }

    public function getMessages()
    {
        return [
            'team_1_id' => [
                'required' => 'Zespół 1 jest wymagany',
            ],
            'team_2_id' => [
                'required' => 'Zespół 2 jest wymagany',
            ],
            'date' => [
                'required' => 'Data jest wymagana',
            ],
            'time' => [
                'required' => 'Godzina jest wymagana',
            ],
            'category_id' => [
                'required' => 'Kategoria jest wymagana',
            ],
        ];
    }
}