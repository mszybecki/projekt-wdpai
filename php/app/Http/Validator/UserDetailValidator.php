<?php

namespace App\Http\Validator;

use Framework\Support\Validator\Validator;

class UserDetailValidator extends Validator
{
    public function getRules()
    {
        return [
            'firstname' => [
                'required',
            ],
            'lastname' => [
                'required',
            ],
        ];
    }
}