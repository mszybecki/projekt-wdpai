<?php

namespace App\Http\Validator;

use Framework\Support\Validator\Validator;

class RegisterValidator extends Validator
{
    public function getRules()
    {
        return [
            'login' => [
                'required',
                'unique:users',
            ],
            'email' => [
                'required',
                'mail',
                'unique:users',
            ],
            'password' => [
                'required',
            ],
            'repeat-password' => [
                'required',
                'same:password',
            ],
        ];
    }

    public function getMessages()
    {
        return [
            'login' =>[
                'required' => 'Login jest wymagany',
                'unique' => 'Taki login już istnieje',
            ],
            'email' =>[
                'required' => 'Email jest wymagany',
                'mail' => 'Email musi być formatu email',
                'unique' => 'Taki email już istnieje',
            ],
            'password' =>[
                'required' => 'Hasło jest wymagane',
            ],
            'repeat-password' =>[
                'required' => 'Powtórzenie hasła jest wymagane',
                'same' => 'Hasła różnią się od siebie',
            ],
        ];
    }
}