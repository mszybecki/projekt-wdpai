<?php

namespace App\Http\Validator;

use Framework\Support\Validator\Validator;

class TeamValidator extends Validator
{
    public function getRules()
    {
        return [
            'name' => [
                'required',
                'unique:teams',
            ]
        ];
    }

    public function getMessages()
    {
        return [
            'name' => [
                'required' => 'Nazwa zespołu jest wymagana',
                'unique' => 'Taka nazwa już istnieje',
            ]
        ];
    }
}