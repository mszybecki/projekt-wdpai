<?php

namespace App\Http\Controller;

use App\Entity\UserDetail;
use App\Http\Validator\UserDetailValidator;
use App\Repository\Interfaces\{
    UserDetailRepositoryInterface,
    UserRepositoryInterface,
};
use Framework\Facade\Auth;
use Framework\Facade\Response;

class HelloController
{
    private UserRepositoryInterface $userRepositoryInterface;

    private UserDetailRepositoryInterface $userDetailRepository;

    public function __construct(UserDetailRepositoryInterface $userDetailRepository, UserRepositoryInterface $userRepositoryInterface)
    {
        $this->userDetailRepository = $userDetailRepository;
        $this->userRepositoryInterface = $userRepositoryInterface;
    }

    public function hello()
    {
        $authenticated = Auth::getAuthenticated();

        return Response::view()
            ->setName('user')
            ->setParameters([
                'user' => $authenticated,
                'vuser' => $this->userRepositoryInterface->view($authenticated->id),
            ]);
        /*$user = Auth::getAuthenticated();

        return Response::view()
            ->setName('hello')
            ->setParameters([
                'username' => $user->username,
            ]);*/
    }

    public function update(UserDetailValidator $validator, $id)
    {
        $validated = $validator->validate();

        $userDetail = new UserDetail();
        $userDetail->id = $id;
        $userDetail->firstname = $validated['firstname'];
        $userDetail->lastname = $validated['lastname'];

        $this->userDetailRepository->save($userDetail);

        return Response::redirect()
            ->setRedirect('/user');
    }
}