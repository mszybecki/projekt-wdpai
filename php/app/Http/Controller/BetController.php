<?php

namespace App\Http\Controller;

use App\Entity\UserBet;
use App\Http\Validator\UserBetValidator;
use App\Repository\Interfaces\BetRepositoryInterface;
use App\Repository\Interfaces\CategoryRepositoryInterface;
use App\Repository\Interfaces\UserBetRepositoryInterface;
use Framework\Facade\Auth;
use Framework\Facade\Response;
use Framework\Core\Response\View;

class BetController
{
    private CategoryRepositoryInterface $categoryRepository;

    private BetRepositoryInterface $betRepository;

    private UserBetRepositoryInterface $userBetRepository;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        BetRepositoryInterface $betRepository,
        UserBetRepositoryInterface $userBetRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->betRepository = $betRepository;
        $this->userBetRepository = $userBetRepository;
    }

    public function index(): View
    {
        return Response::view()
            ->setName('bet')
            ->setParameters([
                'categories' => $this->categoryRepository->all(),
                'vbets' => $this->groupBets($this->betRepository->viewAll(5)),
            ]);
    }

    public function indexBySlug($slug)
    {
        $category = $this->categoryRepository->findBySlug($slug);

        if (!$category) {
            return Response::view()
                ->setStatusCode(404);
        }

        return Response::view()
            ->setName('bet')
            ->setParameters([
                'categories' => $this->categoryRepository->all(),
                'vbets' => $this->groupBets($this->betRepository->viewFindByCategory($category->id, 5)),
            ]);
    }

    private function groupBets($bets = [])
    {
        $result = [];

        foreach ($bets as $bet) {
            if (!array_key_exists($bet->date, $result)) {
                $result[$bet->date] = [];
            }

            $result[$bet->date][] = $bet;
        }

        return $result;
    }

    public function show($id)
    {
        $bet = $this->betRepository->find($id);

        if (!$bet) {
            return Response::view()
                ->setStatusCode(404);
        }

        return Response::view()
            ->setName('bet_detail')
            ->setParameters([
                'categories' => $this->categoryRepository->all(),
                'bet' => $bet,
                'userBets' => $this->userBetRepository->findByBetId($id, 5),
            ]);
    }

    public function addUserBet(UserBetValidator $validator, $id) {
        $validated = $validator->validate();

        $validated['user_id'] = Auth::getAuthenticated()->id;
        $validated['bet_id'] = $id;

        $userBet = new UserBet();
        foreach ($validated as $key => $value) {
            $userBet->{$key} = $value;
        }

        $this->userBetRepository->create($userBet);

        return Response::redirect()
            ->setRedirect("/zaklady/{$id}");
    }
}