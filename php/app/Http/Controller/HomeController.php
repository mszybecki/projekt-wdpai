<?php

namespace App\Http\Controller;

use Framework\Facade\Response;

class HomeController
{
    public function index()
    {
        return Response::view()
            ->setName('home');
    }
}