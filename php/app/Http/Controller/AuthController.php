<?php

namespace App\Http\Controller;

use App\Entity\User;
use App\Http\Validator\RegisterValidator;
use App\Repository\Interfaces\UserRepositoryInterface;
use Framework\Core\Request;
use Framework\Facade\Auth;
use Framework\Facade\Response;

class AuthController
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * AuthController constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function loginForm()
    {
        return Response::view()
            ->setName('login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt($request->getInput('username'), $request->getInput('password'))) {
            return Response::redirect()
                ->setRedirect('/');
        }

        return Response::redirect()
            ->setRedirect('/login')
            ->setStatusCode(403)
            ->setParameters(['errors' => ['Błędne dane']]);
    }

    public function logout()
    {
        Auth::logout();

        return Response::redirect()
            ->setRedirect('/');
    }

    public function registerForm()
    {
        return Response::view()
            ->setName('register');
    }

    public function register(RegisterValidator $validator)
    {
        $validated = $validator->validate();
        unset($validated['repeat-password']);
        $validated['role'] = 'normal';

        $user = new User();
        foreach ($validated as $key => $value) {
            $user->{$key} = $value;
        }

        $this->userRepository->create($user);
        Auth::attempt($validated['login'], $validated['password']);

        return Response::redirect()
            ->setRedirect('/');
    }
}