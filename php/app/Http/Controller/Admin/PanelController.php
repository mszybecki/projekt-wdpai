<?php

namespace App\Http\Controller\Admin;

use Framework\Facade\Response;

class PanelController
{
    public function index()
    {
        return Response::redirect()
            ->setRedirect('/panel/uzytkownicy');
    }
}