<?php

namespace App\Http\Controller\Admin;

use App\Entity\Bet;
use App\Http\Validator\BetValidator;
use App\Repository\Interfaces\BetRepositoryInterface;
use App\Repository\Interfaces\CategoryRepositoryInterface;
use App\Repository\Interfaces\TeamRepositoryInterface;
use Framework\Core\Request;
use Framework\Facade\Response;

class BetController
{
    private Request $request;

    private BetRepositoryInterface $betRepository;

    private TeamRepositoryInterface $teamRepository;

    private CategoryRepositoryInterface $categoryRepository;

    public function __construct(Request $request, BetRepositoryInterface $betRepository, TeamRepositoryInterface $teamRepository, CategoryRepositoryInterface  $categoryRepository)
    {
        $this->request = $request;
        $this->betRepository = $betRepository;
        $this->teamRepository = $teamRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $page = intval($this->request->getInput('page', 0));
        $perPage = intval($this->request->getInput('per_page', 10));

        $prevPage = $page - 1;
        $nextPage = $page + 1;

        $result = $this->betRepository->allPaginated($page, $perPage);

        $responseParameters = [];
        $responseParameters['bets'] = $result;

        if ($page > 0) {
            $responseParameters['prev'] = "/panel/zaklady?page={$prevPage}&per_page={$perPage}";
        }

        if (!(count($result) < $perPage)) {
            $responseParameters['next'] = "/panel/zaklady?page={$nextPage}&per_page={$perPage}";
        }

        return Response::view()
            ->setName('panel.bet.index')
            ->setParameters($responseParameters);
    }

    public function viewForm()
    {
        return Response::view()
            ->setName('panel.bet.form')
            ->setParameters([
                'teams' => $this->teamRepository->all(),
                'categories' => $this->categoryRepository->all(),
            ]);
    }

    public function store(BetValidator $validator)
    {
        $validated = $validator->validate();

        $bet = new Bet();
        foreach ($validated as $key => $value) {
            $bet->{$key} = $value;
        }

        $this->betRepository->create($bet);
        return Response::redirect()
            ->setRedirect('/panel/zaklady');
    }

    public function delete($id)
    {
        $this->betRepository->delete($id);
        return Response::redirect()
            ->setRedirect('/panel/zaklady');
    }
}