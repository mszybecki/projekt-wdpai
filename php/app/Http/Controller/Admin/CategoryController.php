<?php

namespace App\Http\Controller\Admin;

use App\Entity\Category;
use App\Http\Validator\CategoryValidator;
use App\Repository\Interfaces\CategoryRepositoryInterface;
use Framework\Core\Request;
use Framework\Facade\Response;

class CategoryController
{
    private Request $request;

    private CategoryRepositoryInterface $categoryRepository;

    public function __construct(Request $request, CategoryRepositoryInterface $categoryRepository)
    {
        $this->request = $request;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $page = intval($this->request->getInput('page', 0));
        $perPage = intval($this->request->getInput('per_page', 10));

        $prevPage = $page - 1;
        $nextPage = $page + 1;

        $result = $this->categoryRepository->allPaginated($page, $perPage);

        $responseParameters = [];
        $responseParameters['categories'] = $result;

        if ($page > 0) {
            $responseParameters['prev'] = "/panel/kategorie?page={$prevPage}&per_page={$perPage}";
        }

        if (!(count($result) < $perPage)) {
            $responseParameters['next'] = "/panel/kategorie?page={$nextPage}&per_page={$perPage}";
        }

        return Response::view()
            ->setName('panel.category.index')
            ->setParameters($responseParameters);
    }

    public function viewForm()
    {
        return Response::view()
            ->setName('panel.category.form');
    }

    public function store(CategoryValidator $validator)
    {
        $validated = $validator->validate();

        $category = new Category();
        foreach ($validated as $key => $value) {
            $category->{$key} = $value;
        }

        $this->categoryRepository->create($category);
        return Response::redirect()
            ->setRedirect('/panel/kategorie');
    }

    public function delete($id)
    {
        $this->categoryRepository->delete($id);
        return Response::redirect()
            ->setRedirect('/panel/kategorie');
    }
}