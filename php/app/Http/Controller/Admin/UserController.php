<?php

namespace App\Http\Controller\Admin;

use App\Repository\Interfaces\UserRepositoryInterface;
use Framework\Core\Request;
use Framework\Facade\Response;

class UserController
{
    private Request $request;

    private UserRepositoryInterface $userRepository;

    public function __construct(Request $request, UserRepositoryInterface $userRepository)
    {
        $this->request = $request;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $page = intval($this->request->getInput('page', 0));
        $perPage = intval($this->request->getInput('per_page', 10));

        $prevPage = $page - 1;
        $nextPage = $page + 1;

        $result = $this->userRepository->allPaginated($page, $perPage);

        $responseParameters = [];
        $responseParameters['users'] = $result;

        if ($page > 0) {
            $responseParameters['prev'] = "/panel/uzytkownicy?page={$prevPage}&per_page={$perPage}";
        }

        if (!(count($result) < $perPage)) {
            $responseParameters['next'] = "/panel/uzytkownicy?page={$nextPage}&per_page={$perPage}";
        }

        return Response::view()
            ->setName('panel.user')
            ->setParameters($responseParameters);
    }

    public function delete($id)
    {
        $this->userRepository->delete($id);
        return Response::redirect()
            ->setRedirect('/panel/uzytkownicy');
    }

    public function changeRole($id)
    {
        $user = $this->userRepository->get($id);

        if (!$user) {
            return Response::redirect()
                ->setStatusCode(404)
                ->setRedirect('/panel/uzytkownicy');
        }

        $user->role = ($user->role == "admin") ? "normal" : "admin";
        $this->userRepository->update($user);

        return Response::redirect()
            ->setRedirect('/panel/uzytkownicy');
    }
}