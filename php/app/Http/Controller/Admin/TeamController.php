<?php

namespace App\Http\Controller\Admin;

use App\Entity\Team;
use App\Http\Validator\TeamValidator;
use App\Repository\Interfaces\TeamRepositoryInterface;
use Framework\Core\Request;
use Framework\Facade\Response;

class TeamController
{
    private Request $request;

    private TeamRepositoryInterface $teamRepository;

    public function __construct(Request $request, TeamRepositoryInterface $teamRepository)
    {
        $this->request = $request;
        $this->teamRepository = $teamRepository;
    }

    public function index()
    {
        $page = intval($this->request->getInput('page', 0));
        $perPage = intval($this->request->getInput('per_page', 10));

        $prevPage = $page - 1;
        $nextPage = $page + 1;

        $result = $this->teamRepository->allPaginated($page, $perPage);

        $responseParameters = [];
        $responseParameters['teams'] = $result;

        if ($page > 0) {
            $responseParameters['prev'] = "/panel/zespoly?page={$prevPage}&per_page={$perPage}";
        }

        if (!(count($result) < $perPage)) {
            $responseParameters['next'] = "/panel/zespoly?page={$nextPage}&per_page={$perPage}";
        }

        return Response::view()
            ->setName('panel.team.index')
            ->setParameters($responseParameters);
    }

    public function viewForm()
    {
        return Response::view()
            ->setName('panel.team.form');
    }

    public function store(TeamValidator $validator)
    {
        $validated = $validator->validate();

        $team = new Team();
        foreach ($validated as $key => $value) {
            $team->{$key} = $value;
        }

        $this->teamRepository->create($team);
        return Response::redirect()
            ->setRedirect('/panel/zespoly');
    }

    public function delete($id)
    {
        $this->teamRepository->delete($id);
        return Response::redirect()
            ->setRedirect('/panel/zespoly');
    }
}