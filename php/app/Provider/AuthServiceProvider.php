<?php

namespace App\Provider;

use App\Entity\User;
use App\Repository\AuthorizationRepository;
use Framework\Auth\AuthorizationRepositoryInterface;
use Framework\Core\Registry\AliasesRegistry;
use Framework\Core\ServiceProvider;
use Framework\Facade\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * @var AliasesRegistry
     */
    private AliasesRegistry $aliasesRegistry;

    /**
     * AuthServiceProvider constructor.
     * @param AliasesRegistry $aliasesRegistry
     */
    public function __construct(AliasesRegistry $aliasesRegistry)
    {
        $this->aliasesRegistry = $aliasesRegistry;
    }

    public function boot()
    {
        $this->aliasesRegistry->add(AuthorizationRepositoryInterface::class, AuthorizationRepository::class);
    }

    public function register()
    {
        Auth::model(User::class);
        Auth::redirect('/login');
    }
}