<?php

namespace App\Provider;

use App\Repository\Implementations\{
    BetRepository,
    CategoryRepository,
    TeamRepository,
    UserRepository,
    UserDetailRepository,
    UserBetRepository,
};
use App\Repository\Interfaces\{
    BetRepositoryInterface,
    CategoryRepositoryInterface,
    TeamRepositoryInterface,
    UserRepositoryInterface,
    UserDetailRepositoryInterface,
    UserBetRepositoryInterface,
};
use Framework\Core\Registry\AliasesRegistry;
use Framework\Core\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * @var AliasesRegistry
     */
    private AliasesRegistry $aliasesRegistry;

    /**
     * AuthServiceProvider constructor.
     * @param AliasesRegistry $aliasesRegistry
     */
    public function __construct(AliasesRegistry $aliasesRegistry)
    {
        $this->aliasesRegistry = $aliasesRegistry;
    }

    public function boot()
    {
        $this->aliasesRegistry->add(UserRepositoryInterface::class, UserRepository::class);
        $this->aliasesRegistry->add(UserDetailRepositoryInterface::class, UserDetailRepository::class);
        $this->aliasesRegistry->add(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->aliasesRegistry->add(BetRepositoryInterface::class, BetRepository::class);
        $this->aliasesRegistry->add(TeamRepositoryInterface::class, TeamRepository::class);
        $this->aliasesRegistry->add(UserBetRepositoryInterface::class, UserBetRepository::class);
    }

    public function register()
    {

    }
}