<?php

namespace App\Provider;

use Framework\Core\Container;
use Framework\Core\Database\Driver;
use Framework\Core\Registry\AliasesRegistry;
use Framework\Core\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * @var AliasesRegistry
     */
    private AliasesRegistry $aliasesRegistry;

    /**
     * @var Container
     */
    private Container $container;

    /**
     * AuthServiceProvider constructor.
     * @param AliasesRegistry $aliasesRegistry
     * @param Container $container
     */
    public function __construct(AliasesRegistry $aliasesRegistry, Container $container)
    {
        $this->aliasesRegistry = $aliasesRegistry;
        $this->container = $container;
    }

    public function boot()
    {
        $this->aliasesRegistry->add(Driver::class, config('database.driver'));
        $this->container->make(Driver::class)->connect();
    }

    public function register()
    {
    }
}