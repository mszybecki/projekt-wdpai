<?php

namespace App\Repository\Implementations;

use App\Entity\User;
use App\Entity\VUser;
use App\Repository\Interfaces\UserRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Traits\Database\NormalizeResult;

class UserRepository implements UserRepositoryInterface
{
    use NormalizeResult;

    /**
     * @var Driver
     */
    private Driver $driver;

    /**
     * UserRepository constructor.
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function create(User $user)
    {
        $builder = Builder::insertBuilder()
            ->table('users');

        foreach ($user->toDb() as $key => $value) {
            $builder->set($key, $value);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $this->driver->query($query, $params);
    }

    public function allPaginated($page = 0, $perPage = 15)
    {
        $last = $page * $perPage;

        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->select("*")
            ->table('users')
            ->order('id')
            ->limit($perPage)
            ->offset($last)
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, User::class, true);
    }

    public function delete(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::deleteBuilder()
            ->table('users')
            ->where('id', $id)
            ->compile();

        $this->driver->query($query, $params);
    }

    public function get(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->table('users')
            ->select("*")
            ->where('id', $id)
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, User::class);
    }

    public function update(User $user)
    {
        $builder = Builder::updateBuilder()
            ->table('users')
            ->where('id', $user->id);

        $array = $user->toDb();
        unset($array['id']);

        foreach ($user->toDb() as $key => $value) {
            $builder->set($key, $value);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $this->driver->query($query, $params);
    }

    public function view(int $id): VUser
    {
        [
            'query' => $query,
            'params' => $params,
        ] = Builder::selectBuilder()
            ->table('vusers')
            ->select('*')
            ->where('id', $id)
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, VUser::class);
    }
}