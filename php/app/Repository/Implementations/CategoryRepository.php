<?php

namespace App\Repository\Implementations;

use App\Entity\Category;
use App\Repository\Interfaces\CategoryRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Traits\Database\NormalizeResult;
use Framework\Traits\Database\Slugify;

class CategoryRepository implements CategoryRepositoryInterface
{
    use Slugify;
    use NormalizeResult;

    /**
     * @var Driver
     */
    private Driver $driver;

    /**
     * CategoryRepository constructor.
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @return Category[]
     */
    public function all(): array
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->table('categories')
            ->select('*')
            ->order('id')
            ->compile();

        $returned = $this->driver->query($query, $params);
        $result = [];

        foreach ($returned as $value) {
            $user = new Category();
            $user->fromDb($value);

            $result[] = $user;
        }

        return $result;
    }

    public function create(Category $category)
    {
        $builder = Builder::insertBuilder()
            ->table('categories');

        foreach ($category->toDb() as $key => $value) {
            $builder->set($key, $value);
        }

        $builder->set('slug', $this->slugify($category->name));

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $this->driver->query($query, $params);
    }

    public function findBySlug(string $slug)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->table('categories')
            ->select('*')
            ->where('slug', $slug)
            ->compile();

        $returned = $this->driver->query($query, $params);

        return $this->normalizeResult($returned, Category::class);
    }

    public function findById(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->table('categories')
            ->select('*')
            ->where('id', $id)
            ->compile();

        $returned = $this->driver->query($query, $params);

        return $this->normalizeResult($returned, Category::class);
    }

    public function allPaginated($page = 0, $perPage = 15)
    {
        $last = $page * $perPage;

        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->select("*")
            ->table('categories')
            ->limit($perPage)
            ->offset($last)
            ->order('id')
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Category::class, true);
    }

    public function delete(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::deleteBuilder()
            ->table('categories')
            ->where('id', $id)
            ->compile();

        $this->driver->query($query, $params);
    }
}