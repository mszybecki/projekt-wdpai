<?php

namespace App\Repository\Implementations;

use App\Entity\Bet;
use App\Entity\VBet;
use App\Repository\Interfaces\BetRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Traits\Database\NormalizeResult;

class BetRepository implements BetRepositoryInterface
{
    use NormalizeResult;

    /**
     * @var Driver
     */
    private Driver $driver;

    /**
     * CategoryRepository constructor.
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function findByCategory($categoryId, $limit = 0)
    {
        $builder = Builder::selectBuilder()
            ->table('bets')
            ->select('*')
            ->where('category_id', $categoryId);

        if ($limit != 0) {
            $builder->limit($limit);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Bet::class, true);
    }

    public function viewFindByCategory($categoryId, $limit = 0)
    {
        $builder = Builder::selectBuilder()
            ->table('vbets')
            ->select('*')
            ->where('category_id', $categoryId);

        if ($limit != 0) {
            $builder->limit($limit);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, VBet::class, true);
    }

    public function all($limit = 0)
    {
        $builder = Builder::selectBuilder()
            ->table('bets')
            ->select('*')
            ->order('id');

        if ($limit != 0) {
            $builder->limit($limit);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Bet::class);
    }

    public function viewAll($limit = 0)
    {
        $builder = Builder::selectBuilder()
            ->table('vbets')
            ->select('*')
            ->order('id');

        if ($limit != 0) {
            $builder->limit($limit);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Bet::class);
    }

    public function allPaginated($page = 0, $perPage = 15)
    {
        $last = $page * $perPage;

        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->select("*")
            ->table('bets')
            ->order('id')
            ->limit($perPage)
            ->offset($last)
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Bet::class, true);
    }

    public function delete(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::deleteBuilder()
            ->table('bets')
            ->where('id', $id)
            ->compile();

        $this->driver->query($query, $params);
    }

    public function create(Bet $bet)
    {
        $builder = Builder::insertBuilder()
            ->table('bets');

        foreach ($bet->toDb() as $key => $value) {
            $builder->set($key, $value);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $this->driver->query($query, $params);
    }

    public function find(int $id)
    {
        $builder = Builder::selectBuilder()
            ->table('bets')
            ->select('*')
            ->where('id', $id);

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Bet::class);
    }
}