<?php

namespace App\Repository\Implementations;

use App\Entity\Team;
use App\Repository\Interfaces\TeamRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Traits\Database\NormalizeResult;

class TeamRepository implements TeamRepositoryInterface
{
    use NormalizeResult;

    /**
     * @var Driver
     */
    private Driver $driver;

    /**
     * CategoryRepository constructor.
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function all()
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->table('teams')
            ->select("*")
            ->order('id')
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Team::class, true);
    }

    public function allPaginated($page = 0, $perPage = 15)
    {
        $last = $page * $perPage;

        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->select("*")
            ->table('teams')
            ->order('id')
            ->limit($perPage)
            ->offset($last)
            ->compile();

        $result = $this->driver->query($query, $params);

        return $this->normalizeResult($result, Team::class, true);
    }

    public function findById(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::selectBuilder()
            ->table('teams')
            ->select('*')
            ->where('id', $id)
            ->compile();

        $returned = $this->driver->query($query, $params);

        return $this->normalizeResult($returned, Team::class);
    }

    public function create(Team $team)
    {
        $builder = Builder::insertBuilder()
            ->table('teams');

        foreach ($team->toDb() as $key => $value) {
            $builder->set($key, $value);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $this->driver->query($query, $params);
    }

    public function delete(int $id)
    {
        [
            'query' => $query,
            'params' => $params
        ] = Builder::deleteBuilder()
            ->table('teams')
            ->where('id', $id)
            ->compile();

        $this->driver->query($query, $params);
    }
}