<?php

namespace App\Repository\Implementations;


use App\Entity\UserDetail;
use App\Repository\Interfaces\UserDetailRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Traits\Database\NormalizeResult;

class UserDetailRepository implements UserDetailRepositoryInterface
{
    use NormalizeResult;

    private Driver $driver;

    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function save(UserDetail $userDetail)
    {
        [
            'query' => $query,
            'params' => $params,
        ] = Builder::selectBuilder()
            ->select('*')
            ->table('user_details')
            ->where('id', $userDetail->id)
            ->compile();

        $result = $this->driver->query($query, $params);
        if (empty($result)) {
            $builder = Builder::insertBuilder()
                ->table('user_details')
                ->set('id', $userDetail->id);

        } else {
            $builder = Builder::updateBuilder()
                ->table('user_details')
                ->where('id', $userDetail->id);
        }

        [
            'query' => $query,
            'params' => $params,
        ] = $builder->set('firstname', $userDetail->firstname)
            ->set('lastname', $userDetail->lastname)
            ->compile();

        $this->driver->query($query, $params);
    }
}