<?php

namespace App\Repository\Implementations;

use App\Entity\UserBet;
use App\Repository\Interfaces\UserBetRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Traits\Database\NormalizeResult;

class UserBetRepository implements UserBetRepositoryInterface
{
    use NormalizeResult;
    
    private Driver $driver;

    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function findByBetId(int $betId, int $limit = 0)
    {
        [
            'query' => $query,
            'params' => $params,
        ] = Builder::selectBuilder()
            ->table('user_bets')
            ->select('*')
            ->where('bet_id', $betId)
            ->limit(0)
            ->order('id', 'desc')
            ->compile();
        
        $result = $this->driver->query($query, $params);
        
        return $this->normalizeResult($result, UserBet::class, true);
    }

    public function create(UserBet $userBet)
    {
        $builder = Builder::insertBuilder()
            ->table('user_bets');

        foreach ($userBet->toDb() as $key => $value) {
            $builder->set($key, $value);
        }

        [
            'query' => $query,
            'params' => $params
        ] = $builder->compile();

        $this->driver->query($query, $params);
    }
}