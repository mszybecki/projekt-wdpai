<?php

namespace App\Repository\Interfaces;

use App\Entity\User;

interface UserRepositoryInterface
{
    public function create(User $user);
    public function allPaginated($page = 0, $perPage = 15);
    public function delete(int $id);
    public function get(int $id);
    public function update(User $user);
    public function view(int $id);
}