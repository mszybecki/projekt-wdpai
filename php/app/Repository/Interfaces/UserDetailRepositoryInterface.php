<?php

namespace App\Repository\Interfaces;

use App\Entity\UserDetail;

interface UserDetailRepositoryInterface
{
    public function save(UserDetail $userDetail);
}