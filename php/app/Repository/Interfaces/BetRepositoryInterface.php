<?php

namespace App\Repository\Interfaces;

use App\Entity\Bet;

interface BetRepositoryInterface
{
    public function findByCategory($categoryId, $limit = 0);
    public function viewFindByCategory($categoryId, $limit = 0);
    public function all($limit = 0);
    public function viewAll($limit = 0);
    public function allPaginated($page = 0, $perPage = 15);
    public function delete(int $id);
    public function create(Bet $bet);
    public function find(int $id);
}