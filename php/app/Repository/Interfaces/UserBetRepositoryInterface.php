<?php

namespace App\Repository\Interfaces;

use App\Entity\UserBet;

interface UserBetRepositoryInterface
{
    public function findByBetId(int $betId, int $limit = 0);
    public function create(UserBet $userBet);
}