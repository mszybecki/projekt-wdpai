<?php

namespace App\Repository\Interfaces;

use App\Entity\Team;

interface TeamRepositoryInterface
{
    public function all();
    public function allPaginated($page = 0, $perPage = 15);
    public function findById(int $id);
    public function create(Team $team);
    public function delete(int $id);
}