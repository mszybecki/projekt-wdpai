<?php

namespace App\Repository\Interfaces;

use App\Entity\Category;

interface CategoryRepositoryInterface
{
    /**
     * @return Category[]
     */
    public function all(): array;

    /**
     * @param Category $category
     * @return void
     */
    public function create(Category $category);

    /**
     * @param string $slug
     * @return Category|void
     */
    public function findBySlug(string $slug);

    /**
     * @param int $int
     * @return Category|void
     */
    public function findById(int $int);
    public function allPaginated($page = 0, $perPage = 15);
    public function delete(int $id);
}