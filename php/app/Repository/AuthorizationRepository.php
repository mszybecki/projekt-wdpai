<?php

namespace App\Repository;

use Framework\Auth\AuthorizationRepositoryInterface;
use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Facade\Auth;

class AuthorizationRepository implements AuthorizationRepositoryInterface
{
    /**
     * @var Driver
     */
    private $database;

    /**
     * AuthorizationRepository constructor.
     * @param Driver $database
     */
    public function __construct(Driver $database)
    {
        $this->database = $database;
    }

    public function getByLogin($login)
    {
        return $this->query('login', $login);
    }

    public function getByToken($token)
    {
        return $this->query('token', $token);
    }

    public function setToken($login, $token)
    {
        $query = Builder::updateBuilder()
            ->table('users')
            ->set('token', $token)
            ->where('login', $login)
            ->compile();

        $this->database->query($query['query'], $query['params']);
    }

    private function query($column, $value)
    {
        $model = Auth::getModel();

        $query = Builder::selectBuilder()
            ->table('users')
            ->select('*')
            ->where($column, $value)
            ->compile();

        $data = $this->database->query($query['query'], $query['params']);

        if (count($data) == 0) {
            return false;
        }

        $entity = new $model;
        $entity->fromDb($data[0]);

        return $entity;
    }
}