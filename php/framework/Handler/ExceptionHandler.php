<?php

namespace Framework\Handler;

use Throwable;

interface ExceptionHandler
{
    public function handle(Throwable $throwable);
}