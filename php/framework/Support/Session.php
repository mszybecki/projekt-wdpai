<?php

namespace Framework\Support;

use Framework\Traits\PrivateConstructor;

class Session
{
    use PrivateConstructor;

    public static function start()
    {
        session_start();
    }

    public static function check($key)
    {
        return array_key_exists($key, $_SESSION);
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function get($key)
    {
        return $_SESSION[$key];
    }

    public static function unset($key)
    {
        unset($_SESSION[$key]);
    }

    public static function writeClose()
    {
        session_write_close();
    }

    public static function destroy()
    {
        session_unset();
        session_destroy();
    }

    public static function pull($key)
    {
        $result = static::get($key);
        static::unset($key);

        return $result;
    }
}