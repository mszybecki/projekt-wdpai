<?php

namespace Framework\Support;

use Framework\Traits\PrivateConstructor;

class Cookie
{
    use PrivateConstructor;

    public static function set($name, $value, $time = -1)
    {
        if ($time === -1) {
            $time = time() + 60 * 60 * 24;
        }

        setcookie($name, $value, $time);
    }

    public static function check($key)
    {
        return array_key_exists($key, $_COOKIE);
    }

    public static function get($key)
    {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }

        return false;
    }

    public static function unset($key)
    {
        setcookie($key, null, time() - 60 * 60 * 24);
    }
}