<?php

namespace Framework\Support\Validator;

abstract class Rule
{
    public abstract function message($attribute);
    public abstract function passes($attribute, $value);

    public function additional($param){}
}