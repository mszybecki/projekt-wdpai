<?php

namespace Framework\Support\Validator\Rules;

use Framework\Support\Validator\Rule;

class Required extends Rule
{
    public function message($attribute)
    {
        return "Attribute {$attribute} is required";
    }

    public function passes($attribute, $value)
    {
        return !empty($value);
    }
}