<?php

namespace Framework\Support\Validator\Rules;

use Framework\Core\Request;
use Framework\Support\Validator\Rule;

class Same extends Rule
{
    private Request $request;

    private string $otherAttribute;

    /**
     * Same constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function message($attribute)
    {
        return "{$attribute} and {$this->otherAttribute} are different";
    }

    public function passes($attribute, $value)
    {
        return $value == $this->request->getInput($this->otherAttribute);
    }

    public function additional($otherAttribute)
    {
        $this->otherAttribute = $otherAttribute;
    }
}