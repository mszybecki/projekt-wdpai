<?php

namespace Framework\Support\Validator\Rules;

use Framework\Core\Database\Builder;
use Framework\Core\Database\Driver;
use Framework\Support\Validator\Rule;

class Unique extends Rule
{
    private string $table;

    private Driver $driver;

    /**
     * Unique constructor.
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
    }

    public function message($attribute)
    {
        return "{$attribute} is not unique";
    }

    public function passes($attribute, $value)
    {
        [
            'query' => $query,
            'params' => $params,
        ] = Builder::selectBuilder()
            ->table($this->table)
            ->select($attribute)
            ->where($attribute, $value)
            ->compile();

        return empty($this->driver->query($query, $params));
    }

    public function additional($table)
    {
        $this->table = $table;
    }
}