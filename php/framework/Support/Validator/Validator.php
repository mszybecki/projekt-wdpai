<?php

namespace Framework\Support\Validator;

use Framework\Core\Container;
use Framework\Core\Request;
use Framework\Facade\Response;

abstract class Validator
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * Validator constructor.
     * @param Container $container
     * @param Request $request
     */
    public function __construct(Container $container, Request $request)
    {
        $this->container = $container;
        $this->request = $request;
    }

    public function getRules()
    {
        return [];
    }

    public function getMessages()
    {
        return [];
    }

    public final function validate(): array
    {
        $messages = [];

        $validatorRules = $this->getRules();
        $validatorMessages = $this->getMessages();

        $values = [];

        foreach ($validatorRules as $attribute => $ruleClasses) {
            foreach ($ruleClasses as $ruleClass) {
                $ruleClass = explode(':', $ruleClass);

                $additional = $ruleClass;
                unset($additional[0]);

                $ruleClass = $ruleClass[0];

                if (!empty($additional)) {
                    $this->container->executeObjectMethod($ruleClass, 'additional', $additional);
                }

                $passesCurrent = $this->container->executeObjectMethod($ruleClass, 'passes', [$attribute, $this->request->getInput($attribute)]);
                if (!$passesCurrent) {
                    if (isset($validatorMessages[$attribute]) && isset($validatorMessages[$attribute][$ruleClass])) {
                        $message = $validatorMessages[$attribute][$ruleClass];
                    } else {
                        $message  = $this->container->executeObjectMethod($ruleClass, 'message', [$attribute]);
                    }
                    if (!isset($messages[$attribute])) {
                        $messages[$attribute] = $message;
                    }
                } else {
                    $values[$attribute] = $this->request->getInput($attribute);
                }
            }
        }

        if (!empty($messages)) {
            abort(Response::redirect()
                ->setRedirect($_SERVER['HTTP_REFERER'])
                ->setParameters(['errors' => $messages])
                ->setStatusCode(422));
        }

        return $values;
    }
}