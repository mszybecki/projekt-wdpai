<?php

namespace Framework\Support;

use Framework\Traits\PrivateConstructor;

class Hash
{
    use PrivateConstructor;

    /**
     * @param string $str
     * @return bool|string
     */
    public static function make($str)
    {
        $options = [
            'cost' => 12,
        ];

        return password_hash($str, PASSWORD_BCRYPT, $options);
    }

    public static function verify($str, $hash)
    {
        return password_verify($str, $hash);
    }
}