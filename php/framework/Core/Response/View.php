<?php

namespace Framework\Core\Response;

class View extends Response
{
    /**
     * @var string
     */
    private $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return View
     */
    public function setName($name): View
    {
        $this->name = $name;

        return $this;
    }


}