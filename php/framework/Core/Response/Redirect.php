<?php

namespace Framework\Core\Response;

class Redirect extends Response
{
    /**
     * @var string
     */
    protected $redirect;

    /**
     * @return string
     */
    public function getRedirect(): string
    {
        return $this->redirect;
    }

    /**
     * @param string $redirect
     * @return Redirect
     */
    public function setRedirect(string $redirect): Redirect
    {
        $this->redirect = $redirect;
        $this->headers['Location'] = $redirect;

        return $this;
    }
}