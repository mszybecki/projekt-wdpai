<?php

namespace Framework\Core;

use Framework\Exception\RegistryKeyDoesntExists;
use Framework\Exception\RegistryKeyExists;

class Registry
{
    protected $registry = [];

    public function has($key)
    {
        return array_key_exists($key, $this->registry);
    }

    public function add($key, $value)
    {
        if (array_key_exists($key, $this->registry)) {
            throw new RegistryKeyExists($key);
        }

        $this->registry[$key] = $value;
    }

    public function get($key)
    {
        if (!$this->has($key)) {
            throw new RegistryKeyDoesntExists($key);
        }

        return $this->registry[$key];
    }

    public function remove($key)
    {
        unset($key);
    }

    public function dump()
    {
        var_dump($this->registry);
        echo "<br>";
    }
}