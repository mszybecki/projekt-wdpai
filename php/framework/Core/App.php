<?php

namespace Framework\Core;

use Framework\Core\Database\Driver;
use Framework\Core\Registry\AliasesRegistry;
use Framework\Http\Controller\ErrorController;
use Framework\Support\Session;
use RuntimeException;
use Framework\Core\Response\Response as ResponseType;

final class App
{
    /**
     * @var Router
     */
    private Router $router;

    /**
     * @var ViewEngine
     */
    private ViewEngine $viewEngine;

    /**
     * @var ResponseType
     */
    private ResponseType $response;

    /**
     * @var Container
     */
    private Container $container;

    /**
     * @var AliasesRegistry
     */
    private AliasesRegistry $aliasesRegistry;

    /**
     * @var string
     */
    private string $viewBasePath;

    public function __construct(
        Router $router,
        ViewEngine $viewEngine,
        Container $container,
        AliasesRegistry $aliasesRegistry)
    {
        $this->router = $router;
        $this->viewEngine = $viewEngine;
        $this->container = $container;
        $this->aliasesRegistry = $aliasesRegistry;
        $this->viewBasePath = framework_path(['views']);
    }

    public function run()
    {
        $this->aliases();

        $this->init();

        $this->bootProviders();
        $this->registerProviders();

        if ($this->runMiddlewares()) {
            $this->resolve();
        }

        $this->dispatch();
        $this->destroy();
    }

    public function abort(ResponseType $response)
    {
        $this->response = $response;

        $this->dispatch();
        $this->destroy();
        exit;
    }

    private function aliases()
    {
        foreach (config('aliases.class_aliases') as $key => $value) {
            class_alias($value, $key);
        }

        foreach (config('aliases.short_aliases') as $key => $value) {
            $this->aliasesRegistry->add($key, $value);
        }
    }

    private function init()
    {
        Session::start();
    }

    private function destroy()
    {
        $this->container->make(Driver::class)->close();
    }

    private function resolve()
    {
        [
            'controller' => $controller,
            'method' => $method,
            'parameters' => $parameters,
            'middlewares' => $middlewares,
        ] = $this->router->route();


        foreach ($middlewares as $middleware) {
            $response = $this->container->executeObjectMethod($middleware, 'handle');
            if ($response != null) {
                $this->response = $response;
                return;
            }
        }

        $response = $this->container->executeObjectMethod($controller, $method, $parameters);

        if ($controller != ErrorController::class) {
            $this->viewBasePath = base_path(['views']);
        }

        $this->response = $response;
    }

    private function dispatch()
    {
        if ($this->response == null) {
            throw new RuntimeException("Method resolve wasn't called");
        }

        http_response_code($this->response->getStatusCode());

        foreach ($this->response->getHeaders() as $header => $value) {
            if ($header == 'Location' && !empty($this->response->getParameters())) {
                Session::set('redirect_parameters', $this->response->getParameters());
                Session::writeClose();
            }

            header("{$header}: {$value}");
        }

        $viewEngine = $this->container->make(ViewEngine::class);

        $parameters = $this->response->getParameters();
        if (Session::check('redirect_parameters')) {
            $parameters = array_merge($parameters, Session::pull('redirect_parameters'));
        }

        $viewEngine->resolve($this->response->getName(), $parameters, $this->viewBasePath . DIRECTORY_SEPARATOR);
    }

    private function bootProviders()
    {
        $providers = config('providers');

        foreach ($providers as $provider) {
            $this->container->executeObjectMethod($provider, 'boot');
        }
    }

    private function registerProviders()
    {
        $providers = config('providers');

        foreach ($providers as $provider) {
            $this->container->executeObjectMethod($provider, 'register');
        }
    }

    /**
     * Returns false if
     *
     * @return bool
     */
    private function runMiddlewares()
    {
        $middlewares = config('middlewares');

        foreach ($middlewares as $middleware) {
            $result = $this->container->executeObjectMethod($middleware, 'handle');

            if ($result != null) {
                $this->response = $result;
                return false;
            }
        }

        return true;
    }
}