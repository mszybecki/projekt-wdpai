<?php

namespace Framework\Core;

class TemplateEngine
{
    /**
     * @var array
     */
    private $blocks = array();

    /**
     * @var string
     */
    private $cache_path;

    /**
     * @var bool
     */
    private $cache_enabled;

    public function __construct()
    {
        $this->cache_path = config('template.cache_path');
        $this->cache_enabled = config('template.cache_enabled');
    }

    public function view($file, $basePath, $data = array())
    {
        $cached_file = self::cache($file, $basePath);
        extract($data, EXTR_SKIP);

        require $cached_file;
    }

     private function cache($file, $basePath)
     {
        $cached_file = $this->cache_path . str_replace(array('/', '.html', '.'), array('_', '', '_'), $basePath . $file) . '.php';

        if (!$this->cache_enabled || !file_exists($cached_file) || filemtime($cached_file) < filemtime($file)) {
            $code = self::includeFiles($file, $basePath);
            $code = self::compileCode($code);
            file_put_contents($cached_file, '<?php class_exists(\'' . __CLASS__ . '\') or exit; ?>' . PHP_EOL . $code);
        }

        return $cached_file;
    }

    private function clearCache()
    {
        foreach(glob($this->cache_path . '*') as $file) {
            unlink($file);
        }
    }

    private function compileCode($code)
    {
        $code = self::compileBlock($code);
        $code = self::compileYield($code);
        $code = self::compileEscapedEchos($code);
        $code = self::compileEchos($code);
        $code = self::compilePHP($code);
        return $code;
    }

    private function includeFiles($file, $basePath)
    {
        $code = file_get_contents($basePath . str_replace('.', DIRECTORY_SEPARATOR, $file) . '.php');
        preg_match_all('/{% ?(extends|include) ?\'?(.*?)\'? ?%}/i', $code, $matches, PREG_SET_ORDER);
        foreach ($matches as $value) {
            $code = str_replace($value[0], $this->includeFiles($value[2], $basePath), $code);
        }
        $code = preg_replace('/{% ?(extends|include) ?\'?(.*?)\'? ?%}/i', '', $code);
        return $code;
    }

    private function compilePHP($code)
    {
        return preg_replace('~\{%\s*(.+?)\s*\%}~is', '<?php $1 ?>', $code);
    }

    private function compileEchos($code)
    {
        return preg_replace('~\{{\s*(.+?)\s*\}}~is', '<?php echo $1 ?>', $code);
    }

    private function compileEscapedEchos($code)
    {
        return preg_replace('~\{{{\s*(.+?)\s*\}}}~is', '<?php echo htmlentities($1, ENT_QUOTES, \'UTF-8\') ?>', $code);
    }

    private function compileBlock($code)
    {
        preg_match_all('/{% ?block ?(.*?) ?%}(.*?){% ?endblock ?%}/is', $code, $matches, PREG_SET_ORDER);
        foreach ($matches as $value) {
            if (!array_key_exists($value[1], $this->blocks)) $this->blocks[$value[1]] = '';
            if (strpos($value[2], '@parent') === false) {
                $this->blocks[$value[1]] = $value[2];
            } else {
                $this->blocks[$value[1]] = str_replace('@parent', $this->blocks[$value[1]], $value[2]);
            }
            $code = str_replace($value[0], '', $code);
        }
        return $code;
    }

    private function compileYield($code)
    {
        foreach($this->blocks as $block => $value) {
            $code = preg_replace('/{% ?yield ?' . $block . ' ?%}/', $value, $code);
        }
        $code = preg_replace('/{% ?yield ?(.*?) ?%}/i', '', $code);
        return $code;
    }

}