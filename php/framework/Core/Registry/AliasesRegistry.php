<?php

namespace Framework\Core\Registry;

use Framework\Core\Registry;

class AliasesRegistry extends Registry
{
    public function get($key)
    {
        while (array_key_exists($key, $this->registry)) {
            $key = $this->registry[$key];
        }

        return $key;
    }
}