<?php

namespace Framework\Core;

use RuntimeException;

abstract class Facade
{
    public static function getFacadeAccessor()
    {
        throw new RuntimeException("Facade accessor must be set");
    }

    public static function __callStatic($name, $arguments)
    {
        $container = Container::getInstance();
        return $container->executeObjectMethod(static::getFacadeAccessor(), $name, $arguments);
    }
}