<?php

namespace Framework\Core;

abstract class Middleware
{
    public abstract function handle();
}