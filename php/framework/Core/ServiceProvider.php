<?php

namespace Framework\Core;

abstract class ServiceProvider
{
    public abstract function boot();
    public abstract function register();
}