<?php

namespace Framework\Core;

use Exception;
use Framework\Core\Registry\AliasesRegistry;
use Framework\Exception\Container\LoopException;
use Framework\Exception\Container\MethodDoesntExistException;
use ReflectionClass;
use ReflectionNamedType;

final class Container
{
    /**
     * @var AliasesRegistry
     */
    private AliasesRegistry $aliasesRegistry;

    /**
     * @var Registry
     */
    private Registry $makingRegistry;

    /**
     * @var Registry
     */
    private Registry $objectRegistry;

    /**
     * @var self
     */
    private static Container $_instance;

    private function __construct()
    {
        $this->makingRegistry = new Registry();
        $this->objectRegistry = new Registry();

        $this->aliasesRegistry = new AliasesRegistry();
        $this->objectRegistry->add(AliasesRegistry::class, $this->aliasesRegistry);
    }

    /**
     * @return Container
     */
    public static function getInstance() : self
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
            self::$_instance->objectRegistry->add(self::class, self::$_instance);
        }

        return self::$_instance;
    }

    /**
     * Alias can be alias, abstract class or concrete class
     *
     * @param string $alias
     * @return object
     * @throws Exception
     */
    public function make($alias)
    {
        $concrete = $this->aliasesRegistry->get($alias);

        if (!$this->objectRegistry->has($concrete)) {
            $this->objectRegistry->add($concrete, $this->makeObject($concrete));
        }

        return $this->objectRegistry->get($concrete);
    }

    public function executeObjectMethod($class, $method, $params = [])
    {
        $concrete = $this->aliasesRegistry->get($class);
        $instance = $this->make($concrete);

        $reflection = new ReflectionClass($concrete);

        if (!$reflection->hasMethod($method)) {
            throw new MethodDoesntExistException($concrete, $method);
        }

        $methodReflection = $reflection->getMethod($method);
        $parameters = $methodReflection->getParameters();
        $objectsParams = $this->getParamsAsObjects($parameters);

        return $instance->{$method}(...array_merge($objectsParams, $params));
    }

    private function makeObject($class)
    {
        if ($this->makingRegistry->has($class)) {
            throw new LoopException($class);
        }

        $this->makingRegistry->add($class, '');

        $reflection = new ReflectionClass($class);
        $constructor = $reflection->getConstructor();

        if ($constructor) {
            $parameters = $constructor->getParameters();
            $constructorParams = $this->getParamsAsObjects($parameters);

            $instance = new $class(...$constructorParams);
        } else {
            $instance = new $class();
        }

        $this->makingRegistry->remove($class);

        return $instance;
    }

    private function getParamsAsObjects(array $parameters)
    {
        $objects = [];

        foreach ($parameters as $parameter) {
            if ($parameter->hasType() && !$this->isPrimitive($parameter->getType())) {
                $objects[] = $this->make($parameter->getType()->getName());
            }
        }

        return $objects;
    }

    private function isPrimitive($type) {
        $primitives = ['int', 'float', 'bool', 'string'];

        return get_class($type) != ReflectionNamedType::class && in_array($type, $primitives);
    }
}