<?php

namespace Framework\Core;

use Framework\Core\Response\Redirect;
use Framework\Core\Response\Response as ResponseType;
use Framework\Core\Response\View;

class Response
{
    /**
     * @var ResponseType
     */
    private $response;

    /**
     * @return View
     */
    public function view(): View
    {
        $responseType = new View();
        $this->response = $responseType;

        return $responseType;
    }

    /**
     * @return Redirect
     */
    public function redirect(): Redirect
    {
        $responseType = new Redirect();
        $this->response = $responseType;

        return $responseType;
    }

    /**
     * @return ResponseType
     */
    public function getResponse(): ResponseType
    {
        return $this->response;
    }
}