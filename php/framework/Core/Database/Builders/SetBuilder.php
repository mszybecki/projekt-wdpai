<?php

namespace Framework\Core\Database\Builders;

trait SetBuilder
{
    private array $set = [];

    public function set(string $column, $value)
    {
        $this->set[$column] = $value;

        return $this;
    }
}