<?php

namespace Framework\Core\Database\Builders;

class DeleteBuilder
{
    use WhereBuilder;
    use TableBuilder;

    public function compile(): array
    {
        $query = "delete from {$this->table}";

        $params = [];
        if (!empty($this->whereBuilderQuery)) {
            [
                'query' => $queryFromCompiler,
                'params' => $paramsFromCompiler
            ] = $this->compileWhere();

            $query .= " where {$queryFromCompiler}";
            $params = $paramsFromCompiler;
        }

        return [
            'query' => $query,
            'params' => $params,
        ];
    }
}