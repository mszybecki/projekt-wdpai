<?php

namespace Framework\Core\Database\Builders;

class UpdateBuilder
{
    use WhereBuilder;
    use TableBuilder;
    use SetBuilder;

    public function compile(): array
    {
        $query = "update {$this->table} set ";

        $tmp = [];
        $params = [];
        foreach ($this->set as $column => $value) {
            $tmp[] = $column . ' = :' . $column . '';
            $params[":{$column}"] = $value;
        }

        $query .= implode(', ', $tmp);

        if (!empty($this->whereBuilderQuery)) {
            [
                'query' => $queryFromCompiler,
                'params' => $paramsFromCompiler
            ] = $this->compileWhere();

            $query .= " where {$queryFromCompiler}";
            $params = array_merge($params, $paramsFromCompiler);
        }

        return [
            'query' => $query,
            'params' => $params,
        ];
    }
}