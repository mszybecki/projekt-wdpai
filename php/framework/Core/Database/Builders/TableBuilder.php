<?php

namespace Framework\Core\Database\Builders;

trait TableBuilder
{
    /**
     * @var string
     */
    private string $table;

    public function table(string $table)
    {
        $this->table = $table;

        return $this;
    }
}