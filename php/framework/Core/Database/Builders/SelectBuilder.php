<?php

namespace Framework\Core\Database\Builders;

class SelectBuilder
{
    use WhereBuilder;
    use TableBuilder;

    /**
     * @var array
     */
    private array $select = [];

    private int $limit = 0;

    private int $offset = 0;

    private string $order = "";

    public function select(string $select)
    {
        $this->select[] = $select == '*' ? '*' : $select;

        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function offset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    public function order($column, $type = "asc")
    {
        $this->order = "order by {$column} {$type}";

        return $this;
    }

    public function compile(): array
    {
        $query = 'select ';
        $query .= implode(', ', $this->select);
        $query .= ' from ' . $this->table . ' ';

        $params = [];
        if (!empty($this->whereBuilderQuery)) {
            [
                'query' => $queryFromCompiler,
                'params' => $paramsFromCompiler
            ] = $this->compileWhere();

            $query .= " where {$queryFromCompiler}";
            $params = $paramsFromCompiler;
        }

        if ($this->order !== "") {
            $query .= " {$this->order}";
        }

        if ($this->limit !== 0) {
            $query .= " limit {$this->limit}";
        }

        $query .= " offset {$this->offset}";

        return [
            'query' => $query,
            'params' => $params,
        ];
    }
}