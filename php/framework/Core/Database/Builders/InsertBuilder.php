<?php

namespace Framework\Core\Database\Builders;

class InsertBuilder
{
    use TableBuilder;
    use SetBuilder;

    public function compile()
    {
        $query = "insert into {$this->table} (";

        $columns = [];
        $params = [];
        foreach ($this->set as $column => $value) {
            $columns[] = '' . $column . '';
            $params[":{$column}"] = $value;
        }

        $query .= implode(',', $columns);
        $query .= ') values (';
        $query .= implode(',', array_keys($params));
        $query .= ')';

        return [
            'query' => $query,
            'params' => $params
        ];
    }
}