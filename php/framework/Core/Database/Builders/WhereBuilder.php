<?php

namespace Framework\Core\Database\Builders;

trait WhereBuilder
{
    /**
     * @var string
     */
    private string $whereBuilderQuery = '';

    private array $paramsBuilderQuery = [];

    public function where(string $column, $value)
    {
        if ($this->whereBuilderQuery != '') {
            $this->whereBuilderQuery = ' and ';
        }

        $this->whereBuilderQuery = $column . ' = :' . $column . '';
        $this->paramsBuilderQuery[$column] = $value;

        return $this;
    }

    public function whereLessThan(string $column, $value)
    {
        if ($this->whereBuilderQuery != '') {
            $this->whereBuilderQuery = ' and ';
        }

        $this->whereBuilderQuery = $column . ' < :' . $column . '';
        $this->paramsBuilderQuery[$column] = $value;

        return $this;
    }

    private function compileWhere(): array
    {
        return [
            'query' => $this->whereBuilderQuery,
            'params' => $this->paramsBuilderQuery,
        ];
    }
}