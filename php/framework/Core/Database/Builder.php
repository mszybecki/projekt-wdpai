<?php

namespace Framework\Core\Database;

use Framework\Core\Database\Builders\{DeleteBuilder, InsertBuilder, SelectBuilder, UpdateBuilder};

class Builder
{
    public static function selectBuilder(): SelectBuilder
    {
        return new SelectBuilder();
    }

    public static function deleteBuilder(): DeleteBuilder
    {
        return new DeleteBuilder();
    }

    public static function updateBuilder(): UpdateBuilder
    {
        return new UpdateBuilder();
    }

    public static function insertBuilder(): InsertBuilder
    {
        return new InsertBuilder();
    }
}