<?php

namespace Framework\Core\Database;

abstract class Driver
{
    const TRANSACTION_LEVEL_READ_UNCOMMITTED = 0;
    const TRANSACTION_LEVEL_READ_COMMITTED = 1;
    const TRANSACTION_LEVEL_REPEATABLE_READ = 2;
    const TRANSACTION_LEVEL_SERIALIZABLE = 3;

    public abstract function connect();

    /**
     * @param int $level
     * @return mixed
     */
    public abstract function startTransaction(int $level);

    public abstract function commit();

    /**
     * $query in format for example "update sth set column = :value where id = :id"
     * and then to $params pass [":value" => "value", ":id" => 1]
     *
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public abstract function query($query, $params = []);
    public abstract function rollback();
    public abstract function close();
}