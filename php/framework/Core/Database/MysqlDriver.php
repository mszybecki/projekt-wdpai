<?php

namespace Framework\Core\Database;

use PDO;

class MysqlDriver extends Driver
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $database;

    /**
     * @var boolean
     */
    private $autocommit;

    /**
     * @var bool boolean
     */
    private $startedTransaction;

    public function __construct()
    {
        $this->host = config('database.host');
        $this->port = config('database.port');
        $this->user = config('database.user');
        $this->password = config('database.password');
        $this->database = config('database.database');
        $this->autocommit = config('database.autocommit');

        $this->startedTransaction = false;
    }

    public function connect()
    {
        $dns = "mysql" .
            ':host=' . $this->host .
            ((!empty($this->port)) ? (';port=' . $this->port) : '') .
            ';dbname=' .  $this->database;

        // if bad connection exception will be thrown
        $this->connection = new PDO($dns, $this->user, $this->password, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ]);

        $this->connection->query("SET NAMES utf8");
    }

    public function startTransaction(int $level)
    {
        $this->connection->beginTransaction();
        $this->startedTransaction = true;
    }

    public function commit()
    {
        $this->connection->commit();
        $this->startedTransaction = false;
    }

    public function query($query, $params = [])
    {
        $statement = $this->connection->prepare($query);
        $statement->execute($params);

        $rows = $statement->fetchAll();

        $statement->closeCursor();

        return $rows;
    }

    public function rollback()
    {
        $this->connection->rollback();
        $this->startedTransaction = false;
    }

    public function close()
    {
        if ($this->startedTransaction && !$this->autocommit) {
            $this->rollback();
        }

        $this->connection = null;
    }
}