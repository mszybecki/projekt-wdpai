<?php

namespace Framework\Core\Database;

use RuntimeException;

abstract class Model
{
    protected $attributes = [];

    protected array $fillable = [];
    protected $hidden = [];

    protected $id = 'id';

    public final function __get($attribute)
    {
        $methodName = 'get' . $this->normalizeAttributeName($attribute) . 'Attribute';

        if (method_exists($this, $methodName)) {
            return $this->{$methodName}();
        }

        return $this->attributes[$attribute];
    }

    public final function __set($attribute, $value)
    {
        if ($attribute === $this->id && !in_array($this->id, $this->fillable)) {
            throw new RuntimeException('An id cannot be set');
        }

        if (!in_array($attribute, $this->fillable)) {
            throw new RuntimeException($attribute . " is not fillable");
        }

        $methodName = 'set' . $this->normalizeAttributeName($attribute) . 'Attribute';
        if (method_exists($this, $methodName)) {
            $this->{$methodName}($value);
        } else {
            $this->attributes[$attribute] = $value;
        }
    }

    private function normalizeAttributeName(string $attribute)
    {
        $attributes = explode('_', $attribute);

        $sb = '';
        foreach ($attributes as $attr) {
            $sb .= ucfirst(strtolower($attr));
        }

        return $sb;
    }

    public final function toArray()
    {
        $result = [];

        foreach ($this->attributes as $key => $value) {
            $result[$key] = $this->{$key};
        }

        foreach ($this->hidden as $hidden) {
            unset($result[$hidden]);
        }

        return $result;
    }

    public final function fromDb(array $data)
    {
        $this->attributes = $data;
    }

    /**
     * @return array
     */
    public final function toDb()
    {
        return $this->attributes;
    }
}