<?php

namespace Framework\Core;

use Framework\Http\Controller\ErrorController;

class Router
{
    /**
     * @var array
     */
    private $routes;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $parameters = [];

    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->routes = [
            'get' => [],
            'post' => [],
            'patch' => [],
            'delete' => [],
        ];
    }

    public function route() : array
    {
        $currentMethod = $this->request->getMethod();
        $currentPath = $this->request->getPath();

        foreach ($this->routes[$currentMethod] as $route) {
            $pattern = preg_replace('/\/{(.*?)}/', '/(.*?)', $route['route']);

            if (preg_match('#^' . $pattern . '$#', $currentPath, $matches)) {
                $this->parameters = array_slice($matches, 1);

                return $this->routeToArray($route);
            }
        }

        return [
            'controller' => ErrorController::class,
            'method' => 'error',
            'parameters' => [404],
            'middlewares' => [],
        ];
    }

    public function get($route, $what, $middlewares = []) : void
    {
        $this->routes['get'][] = [
            'route' => trim($route, '\/'),
            'what' => $what,
            'middlewares' => $middlewares,
        ];
    }

    public function post($route, $what, $middlewares = []) : void
    {
        $this->routes['post'][] = [
            'route' => trim($route, '\/'),
            'what' => $what,
            'middlewares' => $middlewares,
        ];
    }

    public function patch($route, $what, $middlewares = []) : void
    {
        $this->routes['patch'][] = [
            'route' => trim($route, '\/'),
            'what' => $what,
            'middlewares' => $middlewares,
        ];
    }

    public function delete($route, $what, $middlewares = []) : void
    {
        $this->routes['delete'][] = [
            'route' => trim($route, '\/'),
            'what' => $what,
            'middlewares' => $middlewares,
        ];
    }

    /**
     * @param array $route
     * @return array
     */
    private function routeToArray(array $route): array
    {
        $exploded = explode('@', $route['what']);

        return [
            'controller' => $exploded[0],
            'method' => $exploded[1],
            'parameters' => $this->parameters,
            'middlewares' => $route['middlewares'],
        ];
    }
}