<?php

namespace Framework\Core;

class ViewEngine
{
    /**
     * @var TemplateEngine
     */
    private $templateEngine;

    public function __construct(TemplateEngine $templateEngine)
    {
        $this->templateEngine = $templateEngine;
    }

    public function resolve(string $view, array $parameters, string $basePath)
    {
        $this->templateEngine->view($view, $basePath, $parameters);
    }
}