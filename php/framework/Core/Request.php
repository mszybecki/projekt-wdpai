<?php

namespace Framework\Core;

class Request
{
    /**
     * @var array
     */
    private $inputs;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string.
     */
    private $path;

    public function __construct()
    {
        $this->inputs = $_REQUEST;
        $this->method = strtolower($this->getInput('_method', $_SERVER['REQUEST_METHOD']));
        $this->path = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '\/');
    }

    public function getMethod() : string
    {
        return $this->method;
    }

    public function getPath() : string
    {
        return $this->path;
    }

    public function getInput($key, $default = null)
    {
        if (array_key_exists($key, $this->inputs)) {
            return $this->inputs[$key];
        }

        return $default;
    }

    public function getInputs()
    {
        return $this->inputs;
    }

    public function setInput($key, $value)
    {
        $this->inputs[$key] = $value;
    }
}