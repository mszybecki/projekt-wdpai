<?php

namespace Framework\Exception;

use \RuntimeException;

class RegistryKeyExists extends RuntimeException
{
    public function __construct($key)
    {
        parent::__construct("Key {$key} already exists in registry");
    }
}