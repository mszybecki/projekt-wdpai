<?php

namespace Framework\Exception\Container;

use \RuntimeException;

class LoopException extends RuntimeException
{
    public function __construct($class)
    {
        parent::__construct("Loop exception while making {$class}");
    }
}