<?php

namespace Framework\Exception\Container;

use \RuntimeException;

class MethodDoesntExistException extends RuntimeException
{
    public function __construct($class, $method)
    {
        parent::__construct("In {$class} method {$method} doesnt exist");
    }
}