<?php

namespace Framework\Facade;

use Framework\Auth\Auth as AuthFacadeAccessor;
use Framework\Core\Facade;

/**
 * Class Auth
 * @package Framework\Facade
 * @method static bool attempt(string $username, string $password)
 * @method static void logout()
 * @method static bool authenticated()
 * @method static object getAuthenticated()
 * @method static void model(string $model)
 * @method static string getModel()
 * @method static string redirect(string $url)
 * @method static string getRedirect()
 */
class Auth extends Facade
{
    public static function getFacadeAccessor()
    {
        return AuthFacadeAccessor::class;
    }
}