<?php

namespace Framework\Facade;

use Framework\Core\Facade;
use Framework\Core\Router as RouterCore;

/**
 * Class Router
 * @package Framework\Facade
 * @method static void get($route, $what, $middlewares = [])
 * @method static void post($route, $what, $middlewares = [])
 * @method static void patch($route, $what, $middlewares = [])
 * @method static void delete($route, $what, $middlewares = [])
 */
class Router extends Facade
{
    public static function getFacadeAccessor()
    {
        return RouterCore::class;
    }
}