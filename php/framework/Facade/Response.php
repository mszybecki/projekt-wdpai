<?php

namespace Framework\Facade;

use Framework\Core\Facade;
use Framework\Core\Response as ResponseCore;

/**
 * Class Response
 * @package Framework\Facade
 * @method static ResponseCore\View view()
 * @method static ResponseCore\Redirect redirect()
 */
class Response extends Facade
{
    public static function getFacadeAccessor()
    {
        return ResponseCore::class;
    }
}