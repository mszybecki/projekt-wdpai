<?php

namespace Framework\Auth;

use Framework\Support\Cookie;
use Framework\Support\Hash;

class Auth
{
    /**
     * @var AuthorizationRepositoryInterface
     */
    private $authorizationRepository;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $redirect;

    /**
     * Auth constructor.
     * @param AuthorizationRepositoryInterface $authorizationRepository
     */
    public function __construct(AuthorizationRepositoryInterface $authorizationRepository)
    {
        $this->authorizationRepository = $authorizationRepository;
    }

    /**
     * Tries to login user based on $username and $password
     * using method getByUsername and setToken from AuthorizationRepositoryInterface
     *
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function attempt($username, $password)
    {
        $user = $this->authorizationRepository->getByLogin($username);

        if (!$user) {
            return false;
        }

        if (Hash::verify($password, $user->password)) {
            $token = $this->str_rand(32);
            $this->authorizationRepository->setToken($username, $token);
            Cookie::set('token', $token);

            return true;
        }

        return false;
    }

    /**
     * Logout user
     */
    public function logout()
    {
        if ($this->authenticated()) {
            $token = Cookie::get('token');

            $user = $this->authorizationRepository->getByToken($token);
            $this->authorizationRepository->setToken($user->user, '');

            Cookie::unset('token');
        }
    }

    /**
     * Check if user is logged based on token passed in cookie
     *
     * @return bool
     */
    public function authenticated()
    {
        if (!Cookie::check('token')) {
            return false;
        }

        $user = $this->authorizationRepository->getByToken(Cookie::get('token'));

        if ($user) {
            return true;
        }

        return false;
    }

    /**
     * Returns authenticated user
     *
     * @return object
     */
    public function getAuthenticated()
    {
        if ($this->authenticated()) {
            $token = Cookie::get('token');

            return $this->authorizationRepository->getByToken($token);
        }
    }

    /**
     * Set model class using to authenticate
     *
     * @param string $model
     */
    public function model($model)
    {
        $this->model = $model;
    }

    /**
     * Return model class
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set url to redirect if user is not authenticated
     *
     * @param string $redirect
     */
    public function redirect($redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * Return redirect
     *
     * @return string
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    private function str_rand(int $length = 64){ // 64 = 32
        $length = ($length < 4) ? 4 : $length;
        return bin2hex(random_bytes(($length-($length%2))/2));
    }
}