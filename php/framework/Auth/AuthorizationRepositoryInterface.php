<?php

namespace Framework\Auth;

interface AuthorizationRepositoryInterface
{
    public function getByLogin($login);

    public function getByToken($token);

    public function setToken($login, $token);
}