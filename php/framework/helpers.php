<?php

use Framework\Core\Container;
use Framework\Core\Response\Response;

if (!function_exists('container')) {
    function container(): Container
    {
        return Container::getInstance();
    }
}

if (!function_exists('base_path')) {
    /**
     * @param string|array $path
     * @return string
     */
    function base_path($path)
    {
        $base = $_SERVER['APPLICATION_PATH'];

        if (is_array($path)) {
            foreach ($path as $dir) {
                $base .= DIRECTORY_SEPARATOR . $dir;
            }
        } else {
            $base .= DIRECTORY_SEPARATOR . $path;
        }

        return $base;
    }
}

if (!function_exists('framework_path')) {
    /**
     * @param string|array $path
     * @return string
     */
    function framework_path($path)
    {
        return base_path(array_merge(['framework'], $path));
    }
}

if (!function_exists('env')) {
    /**
     * @param string $param
     * @param string $default
     * @return string
     */
    function env(string $param, string $default = '')
    {
        $env = getenv($param);
        return $env ? $env : $default;
    }
}

if (!function_exists('config')) {
    /**
     * @param string $param
     * @param string $default
     * @return string
     */
    function config(string $param)
    {
        $splitted = explode('.', $param);
        $arr = require base_path(['config', $splitted[0] . '.php']);

        if (count($splitted) == 1) {
            return $arr;
        }

        return $arr[$splitted[1]];
    }
}

if (!function_exists('abort')) {
    function abort(Response $response)
    {
        container()->make('app')->abort($response);
    }
}
