<?php

namespace Framework\Traits;

trait StaticAccess
{
    /**
     * @var self
     */
    private static $_instance;

    public static function getInstance() : self
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}