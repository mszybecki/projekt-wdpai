<?php

namespace Framework\Traits;

use Framework\Core\Container;

trait StaticAccessWithContainer
{
    /**
     * @var self
     */
    private static $_instance;

    public static function getInstance() : self
    {
        if (self::$_instance == null) {
            $container = Container::getInstance();
            self::$_instance = $container->make(self::class);
        }

        return self::$_instance;
    }
}