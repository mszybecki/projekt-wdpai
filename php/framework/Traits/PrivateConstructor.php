<?php

namespace Framework\Traits;

trait PrivateConstructor
{
    private function __construct()
    {

    }
}