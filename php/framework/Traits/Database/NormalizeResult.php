<?php

namespace Framework\Traits\Database;

trait NormalizeResult
{
    protected function normalizeResult($data, string $class, bool $allToArray = false)
    {
        if (!isset($data) || empty($data)) {
            return null;
        }

        if (!$allToArray && count($data) == 1) {
            $model = new $class();
            $model->fromDb($data[0]);

            return $model;
        }

        $result = [];
        foreach ($data as $datum) {
            $model = new $class();
            $model->fromDb($datum);

            $result[] = $model;
        }

        return $result;
    }
}