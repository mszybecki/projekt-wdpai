$(function() {
    $('input').focusin(function() {
        $(this).prev().addClass('active');
    });

    $('input').focusout(function() {
        $(this).prev().removeClass('active');
    });

    const mobileMenuButton = $('#mobile-menu-open');
    const mobileMenu = $('.menu__mobile');
    let mobileMenuActive = false;

    mobileMenuButton.click(function () {
        if (mobileMenuActive) {
            mobileMenu.removeClass('active');
            mobileMenuActive = false;
        } else {
            mobileMenu.addClass('active');
            mobileMenuActive = true;
        }
    });

    const mobileCategoriesButton = $('.bets--categories--mobile__title');
    const mobileCategoriesLinks = $('.bets--categories--links');
    let mobileCategoriesActive = false;

    mobileCategoriesButton.click(function () {
        if (mobileCategoriesActive) {
            mobileCategoriesLinks.removeClass('active');
            mobileCategoriesActive = false;
        } else {
            mobileCategoriesLinks.addClass('active');
            mobileCategoriesActive = true;
        }
    });
});