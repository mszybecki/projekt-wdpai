<?php

error_reporting(E_ALL & ~E_WARNING);

require '../vendor/autoload.php';

use Framework\Core\App;
use Framework\Core\Registry\AliasesRegistry;

$container = container();

$aliasesRegistry = $container->make(AliasesRegistry::class);
$aliasesRegistry->add('app', App::class);

$app = $container->make('app');
$app->run();
